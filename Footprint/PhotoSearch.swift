//
//  PhotoSearch.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 02.09.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import Foundation

struct PhotoSearch {
    let year: Int
    var photos: [Photo]
}