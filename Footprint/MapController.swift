//
//  MapController.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 06.08.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import UIKit
import MapKit
import Photos

class MapController: UIViewController, MKMapViewDelegate, UISearchBarDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var SearchBar: UISearchBar!
    var request:MKLocalSearchRequest!
    var search:MKLocalSearch!
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        print("Searched for: \(searchText)")
        
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        SearchBar.setShowsCancelButton(true, animated: true)
        tableView.hidden = false
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        SearchBar.setShowsCancelButton(false, animated: true)
        SearchBar.text = nil
        SearchBar.resignFirstResponder()
        tableView.hidden = true
    }
    
    @IBOutlet weak var map: MKMapView!
    let assetManagement = AssetManagement()
    var images = [Photo]()
    var pins = [Pin]()
    var found = false
    var finishedLoading = false
    
    let imageSize:CGFloat = 72
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is Pin) {
            return nil
        }
        
        let pin = annotation as! Pin
        pin.title = "Photo"
        pin.subtitle = "my description"
        
        var anView = mapView.dequeueReusableAnnotationViewWithIdentifier(pin.identifier)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: pin.identifier)
        }
        else {
            anView!.annotation = annotation
        }
        let deleteButton = UIButton(type: UIButtonType.DetailDisclosure) as UIButton
        anView?.image = pin.image
        anView?.canShowCallout = true
        anView?.rightCalloutAccessoryView = deleteButton
        
        return anView
    }
    
    func mapView(mapView: MKMapView, didAddAnnotationViews views: [MKAnnotationView]) {
        for view:MKAnnotationView in views{
            addBounceAnnimationToView(view)        }
    }
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if let pin = view.annotation as? Pin {
            performSegueWithIdentifier("showFullView", sender: pin.asset)
        }
    }
    
    func addBounceAnnimationToView(view:UIView)
    {
        let bounceAnimation:CAKeyframeAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        bounceAnimation.values = [(0.05), (1.1), (0.9), (1)]
        
        bounceAnimation.duration = 0.6
        let timingFunctions = NSMutableArray()
        for _ in 0..<4 {
            timingFunctions.addObject(CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn))
        }
        bounceAnimation.timingFunctions = timingFunctions.copy() as? [CAMediaTimingFunction]
        bounceAnimation.removedOnCompletion = false
        
        view.layer.addAnimation(bounceAnimation, forKey: "bounce")
    }
    
    func mapView(mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        if  !finishedLoading{
            return
        }
        
        assignPinsInMap()
    }
    
    var assetsInRegion = [PHAsset]()
    
    func assignPinsInMap(){
        let requestOptions = PHImageRequestOptions()
        requestOptions.resizeMode = .Exact
        requestOptions.networkAccessAllowed = true
        requestOptions.deliveryMode = .HighQualityFormat
        let imgManager = PHImageManager.defaultManager()
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            for asset in self.assetsWithLocation{
                if let coordinate = asset.location?.coordinate{
                    if (self.regionContains(self.map.region, location: coordinate)){
                        imgManager.requestImageForAsset(asset, targetSize: CGSize(width: self.imageSize, height: self.imageSize), contentMode: .AspectFill, options: requestOptions, resultHandler: { (image, _) in
                            if let photo = image{
                                let pin = Pin(coordinate: coordinate)
                                pin.image = photo.circle
                                pin.title = "\(asset.creationDate)"
                                pin.asset = asset
                                
                                if !self.assetsInRegion.contains(asset){
                                    self.assetsInRegion.append(asset)
                                    dispatch_async(dispatch_get_main_queue(), {
                                            self.map.addAnnotation(pin)
                                    })
                                }
                            }
                        })
                    }else{
                        self.removeAssetFromCollection(asset)
                    }
                }
            }
            self.removePinsNotInRegion()
        })
    }
    
    func removeAssetFromCollection(asset:PHAsset){
        if let found = self.assetsInRegion.indexOf(asset) {
            self.assetsInRegion.removeAtIndex(found)
        }
    }
    
    func removePinsNotInRegion(){
        for pin in map.annotations{
            if !self.regionContains(map.region, location: pin.coordinate) && pin.title! != "user" {
                dispatch_async(dispatch_get_main_queue(), {
                    self.map.removeAnnotation(pin)
                })
            }
        }
    }
    
    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation) {
        if !found {
            let pin = Pin(coordinate: map.userLocation.coordinate)
            pin.title = "user"
            map.addAnnotation(pin)
            
            centerMapOnUser()
            found = true
        }
    }
    
    /* confirms that a region contains a location */
    func regionContains(region: MKCoordinateRegion, location: CLLocationCoordinate2D) -> Bool {
        let center   = region.center;
        let factor = 2.5
        var northWestCorner = CLLocationCoordinate2D()
        var southEastCorner = CLLocationCoordinate2D()
        
        northWestCorner.latitude  = center.latitude  - (region.span.latitudeDelta  / factor);
        northWestCorner.longitude = center.longitude - (region.span.longitudeDelta / factor);
        southEastCorner.latitude  = center.latitude  + (region.span.latitudeDelta  / factor);
        southEastCorner.longitude = center.longitude + (region.span.longitudeDelta / factor);
        
        if (
            location.latitude  >= northWestCorner.latitude &&
                location.latitude  <= southEastCorner.latitude &&
                
                location.longitude >= northWestCorner.longitude &&
                location.longitude <= southEastCorner.longitude
            )
        {
            // User location (location) in the region - OK :-)
            return true
            
        }else {
            
            // User location (location) out of the region - NOT ok :-(
            return false
        }
    }
    
    var assetsWithLocation: [PHAsset]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SearchBar.delegate = self
        
        assetManagement.getAssetsWithLocationData({
            (result: [PHAsset]) in
                self.assetsWithLocation = result
                self.finishedLoading = true
        })
    }
    @IBAction func changeMapView(sender: UISegmentedControl) {
        switch (sender.selectedSegmentIndex){
        case 0:
            map.mapType = .Standard
        case 1:
            map.mapType = .Satellite
        case 2:
            map.mapType = .Hybrid
        default:
            map.mapType = .Standard
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showFullView"{
            if let navController = segue.destinationViewController as? UINavigationController{
                if let controller:ViewPhoto = navController.topViewController as? ViewPhoto{
                    if let asset = sender as? PHAsset{
                        controller.asset = asset
                    }
                }
            }
        }
    }
    
    
    func centerMapOnUser(){
        let userLocation = map.userLocation
        
        let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        
        let width = 500.0 //meters
        let height = 500.0
        
        let region = MKCoordinateRegionMakeWithDistance(center, width, height)
        
        map.setRegion(region, animated: true)
        map.setUserTrackingMode(.Follow, animated: true)
    }
}
