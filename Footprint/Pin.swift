//
//  Pin.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 06.08.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import Foundation
import MapKit
import Photos

class Pin : MKPointAnnotation {
    private var _image:UIImage?
    var image:UIImage?
    var asset:PHAsset?
    
    var identifier:String!
    var subView: UIView?
    
    init(coordinate: CLLocationCoordinate2D, image:UIImage?){
        super.init()
        super.coordinate = coordinate
        self.image = image
        self.identifier = NSUUID().UUIDString
    }
    
    init(coordinate: CLLocationCoordinate2D){
        super.init()
        super.coordinate = coordinate
        self.image = nil
        self.identifier = NSUUID().UUIDString
    }
    
    func circularCropImage(image:UIImage, completion:(UIImageView) -> Void){
        let imageView = UIImageView(image: image)
        
        imageView.layer.borderWidth = 3.0
        imageView.layer.borderColor = UIColor.whiteColor().CGColor
        imageView.layer.cornerRadius = image.size.width/2
        imageView.clipsToBounds = true
        
        imageView.layer.contentsGravity = kCAGravityCenter
        imageView.layer.magnificationFilter = kCAFilterLinear
        
        imageView.contentMode = .ScaleAspectFit
        
        completion(imageView)
    }
}
