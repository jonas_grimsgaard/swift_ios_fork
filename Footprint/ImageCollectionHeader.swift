//
//  ImageCollectionHeader.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 01.09.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import UIKit

class ImageCollectionHeader: UICollectionReusableView {
        
    @IBOutlet weak var headerLabel: UILabel!
}
