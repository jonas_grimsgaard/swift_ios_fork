//
//  Bases.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 18.09.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import Foundation


struct TBQuadTreeNodeData {
    var x: Double!
    var y: Double!
    var data: AnyObject?
    
    init(x:Double, y:Double, data: AnyObject?){
        self.x = x
        self.y = y
        self.data = data
    }
}

struct TBoundingBox {
    var x0, y0: Double!
    var xf, yf: Double!
    
    init(x0:Double, y0:Double, xf:Double, yf:Double){
        self.x0 = x0
        self.y0 = y0
        self.xf = xf
        self.yf = yf
    }
}
/*
class quadTreeNode {
    var northWest: quadTreeNode?
    var northEast: quadTreeNode?
    var southWest: quadTreeNode?
    var southEast: quadTreeNode?
    
    var boundingBox : TBoundingBox
    var bucketCapacity: Int
    var points: TBQuadTreeNodeData
    
    init(boundary: TBoundingBox, bucketCapacity:Int){
    }
}*/