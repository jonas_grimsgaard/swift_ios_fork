//
//  PhotoCell.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 02.08.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell{
    
    @IBOutlet weak var photoCell: UIImageView!
    
    func setThumbnailImage(thumbnailImage: UIImage){
        self.photoCell.image = thumbnailImage
    }
}
