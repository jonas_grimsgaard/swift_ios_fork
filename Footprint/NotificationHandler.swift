//
//  NotificationHandler.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 15.09.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import UIKit

class NotificationHandler {
    
    func scheduleNotification(assetIndex:Int, fireDate:NSDate) {
        let notification = UILocalNotification()
        notification.alertBody = "You have memories waiting for you today" // text that will be displayed in the notification
        notification.alertAction = "open" // text that is displayed after "slide to..." on the lock screen - defaults to "slide to view"
        notification.fireDate = fireDate // todo item due date (when notification will be fired)
        notification.soundName = UILocalNotificationDefaultSoundName // play default sound
        notification.userInfo = ["assetIndex": assetIndex, ] // assign a unique identifier to the notification so that we can retrieve it later
        notification.category = "PHOTO_MEMORY"
        UIApplication.sharedApplication().scheduleLocalNotification(notification)
    }
}