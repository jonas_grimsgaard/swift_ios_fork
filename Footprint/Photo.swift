//
//  Photo.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 07.08.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import Foundation
import Photos

class Photo {
    let image: UIImage!
    var location: CLLocation? {
        get{
            if let loc = asset.location {
                return loc
            }
            return nil
        }
    }
    let asset: PHAsset!
    
    init(image:UIImage, asset: PHAsset?){
        self.image = image
        self.asset = asset
    }
    
    var pin: Pin?
}