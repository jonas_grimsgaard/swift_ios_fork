//
//  PhotoInformationViewController.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 19.08.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import UIKit
import Photos
import MapKit

class PhotoInformationViewController: UIViewController, MKMapViewDelegate {
    
    var asset: PHAsset?
    
    override func viewDidLoad() {
        
        let option = PHContentEditingInputRequestOptions()
        option.networkAccessAllowed = true
        
        asset?.requestContentEditingInputWithOptions(option, completionHandler: { (contentEditingInput: PHContentEditingInput?, _) -> Void in
            if let url = contentEditingInput!.fullSizeImageURL{
                let orientation = contentEditingInput!.fullSizeImageOrientation
                var inputImage = CIImage(contentsOfURL: url)
                inputImage = inputImage!.imageByApplyingOrientation(orientation)
                
                if let dict = inputImage?.properties{
                    if let exif = dict["{Exif}"]{
                        if let fnumber = exif["FNumber"] as? Float{
                            let text = String.localizedStringWithFormat("%.1f", fnumber)
                            self.aperture.text = text
                            self.aperture.hidden = false
                        }
                        if let focal = exif["FocalLength"] as? Float{
                            self.focalLength.text = "\(focal)"
                            self.focalLength.hidden = false
                        }
                        if let isoParse = exif["ISOSpeedRatings"] as? NSArray{
                            if let isoValue = isoParse[0] as? Float{
                                self.iso.text = String.localizedStringWithFormat("%.0f", isoValue)
                                self.iso.hidden = false
                            }
                            
                        }
                        self.lensModel.text = exif["LensModel"] as? String
                        if let shutterValue = exif["ShutterSpeedValue"] as? Float{
                            self.exposure.text = "\(shutterValue)"
                            self.exposure.hidden = false
                        }
                    }
                    if let exifAux = dict["{ExifAux}"]{
                        if self.lensModel.text == nil{
                            self.lensModel.text = exifAux["LensModel"] as? String
                            self.lensModel.hidden = false
                        }
                    }
                    if let tiff = dict["{TIFF}"]{
                        self.cameraMaker.text =  tiff["Make"] as? String
                        self.cameraMaker.hidden = false
                        self.cameraModel.text = tiff["Model"] as? String
                        self.cameraModel.hidden = false
                    }
                }
                
                for (key, value) in inputImage!.properties {
                    print("key: \(key)")
                    print("value: \(value)")
                    print("Location: \(self.asset?.location)")
                }
            }
        })
        
        if let date = asset?.creationDate{
            let formatter = NSDateFormatter()
            formatter.dateFormat = "y-MM-dd HH:mm:ss"
            
            dateTimeTaken.text = formatter.stringFromDate(date)
            dateTimeTaken.hidden = false
        }
        
        if let location = asset?.location{
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            
            let meters = 500000.0 //meters
            
            let width = meters
            let height = meters
            
            let region = MKCoordinateRegionMakeWithDistance(center, width, height)
            
            mapView.setRegion(region, animated: true)
            
            mapView.addAnnotation(Pin(coordinate: location.coordinate))
        }
        else{
            
        }
        let requestOptions = PHImageRequestOptions()
        requestOptions.networkAccessAllowed = true
        
        PHImageManager.defaultManager().requestImageForAsset(asset!, targetSize: CGSize(width: 100, height: 100), contentMode: PHImageContentMode.AspectFill, options: requestOptions, resultHandler: { (image, _) in
            self.imageThumbnail.image = image
        })
    }
    
    @IBAction func sharePhotoAction(sender: AnyObject) {
        let sharing = SharingAction()
        
        if let imageAsset = asset{
            sharing.shareAction(imageAsset, viewController: self)
        }
    }
    
    @IBAction func onSwiping(sender: AnyObject) {
        if let allViewControllers = self.navigationController?.viewControllers{
            for aViewController in allViewControllers{
                if aViewController is ViewPhoto{
                    self.navigationController?.popToViewController(aViewController, animated: false)
                    aViewController.dismissViewControllerAnimated(true, completion: nil)
                }
            }
        }
    }
    
    @IBOutlet weak var cameraMaker: UILabel!
    @IBOutlet weak var cameraModel: UILabel!
    @IBOutlet weak var lensModel: UILabel!
    @IBOutlet weak var exposure: UILabel!
    @IBOutlet weak var dateTimeTaken: UILabel!
    @IBOutlet weak var focalLength: UILabel!
    @IBOutlet weak var aperture: UILabel!
    @IBOutlet weak var iso: UILabel!
    @IBOutlet weak var imageThumbnail: UIImageView!
    @IBOutlet weak var mapView: MKMapView!
}
