//
//  ViewPhoto.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 02.08.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import UIKit
import Photos

class ViewPhoto: UIViewController, UIScrollViewDelegate {
    
    @IBAction func Cancel(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var scrollView: UIScrollView!

    @IBAction func handleTap(sender: AnyObject) {
        if(hidden){
            hidden = false;
            fadeBackground(UIColor.whiteColor())
            UIApplication.sharedApplication().statusBarHidden = hidden;
        }
        else{
            hidden = true
            fadeBackground(UIColor.blackColor())
            UIApplication.sharedApplication().statusBarHidden = hidden;
        }
        
        self.navigationController?.setNavigationBarHidden(hidden, animated: true)
        self.navigationController?.setToolbarHidden(hidden, animated: true)
    }
    
    func fadeBackground(toColor: UIColor){
        UIView.animateWithDuration(0.3, delay: 0, options: UIViewAnimationOptions.CurveLinear, animations: {
            self.view.backgroundColor = toColor
            }) { (Bool) -> Void in
        }
    }
    
    @IBAction func shareAction(sender: AnyObject) {
        let shareAction = SharingAction()
        
        if asset != nil{
            shareAction.shareAction(asset, viewController: self)
        }
    }
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var toolBar: UIToolbar!
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.hidesBarsOnTap = true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "photoInformation" {
            if let controller = segue.destinationViewController as? PhotoInformationViewController{
                controller.asset = asset
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewPhoto.rotated), name: UIDeviceOrientationDidChangeNotification, object: nil)
        
        loadFullSizeImage()
        
        setupTapGesturizer()
        
        setupPanGesture()
        
        center = self.view.center
    }
    
    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    func rotated()
    {
        setZoomScale()
    }
    
    func setZoomScale() {
        if let _ = imageView{
            if let image = imageView.image{
                let imageViewSize = image.size
                let scrollViewSize = scrollView.bounds.size
                let widthScale = scrollViewSize.width / imageViewSize.width
                let heightScale = scrollViewSize.height / imageViewSize.height
                
                let minZoomScale = min(widthScale, heightScale)
                scrollView.minimumZoomScale = minZoomScale
                scrollView.zoomScale = minZoomScale
            }
        }
    }
    
    func scrollViewDidZoom(scrollView: UIScrollView) {
        let imageViewSize = imageView.frame.size
        let scrollViewSize = scrollView.bounds.size
        
        let verticalPadding = imageViewSize.height < scrollViewSize.height ? (scrollViewSize.height - imageViewSize.height) / 2 : 0
        let horizontalPadding = imageViewSize.width < scrollViewSize.width ? (scrollViewSize.width - imageViewSize.width) / 2 : 0
        
        scrollView.contentInset = UIEdgeInsets(top: verticalPadding, left: horizontalPadding, bottom: verticalPadding, right: horizontalPadding)
    }
    
    var center: CGPoint!
    
    func handlePanGesture(gestureRecognizer: UIPanGestureRecognizer){
        if gestureRecognizer.state == UIGestureRecognizerState.Began || gestureRecognizer.state == UIGestureRecognizerState.Changed {
            let translation = gestureRecognizer.translationInView(self.view)
            if center.y < (gestureRecognizer.view!.center.y + translation.y){
                gestureRecognizer.view!.center = CGPointMake(gestureRecognizer.view!.center.x, gestureRecognizer.view!.center.y + translation.y)
                gestureRecognizer.setTranslation(CGPointMake(0,0), inView: self.view)
            }
        }
        if gestureRecognizer.state == UIGestureRecognizerState.Ended{
            if ( gestureRecognizer.view!.center.y > (self.center.y * 0.75))
            {
                UIApplication.sharedApplication().statusBarHidden = false
                self.dismissViewControllerAnimated(true, completion: nil)
            }
            UIView.animateWithDuration(0.3, animations: {
                gestureRecognizer.view!.center = CGPointMake(self.center.x, self.center.y)
                gestureRecognizer.setTranslation(CGPointMake(0,0), inView: self.view)
            })
        }
    }
    
    func setupPanGesture(){
        let panning = UIPanGestureRecognizer(target: self, action: #selector(ViewPhoto.handlePanGesture(_:)))
        self.navigationController?.view.addGestureRecognizer(panning)
    }
    
    func setupGestureRecognizer() {
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(ViewPhoto.handleDoubleTap(_:)))
        doubleTap.numberOfTapsRequired = 2
        scrollView.addGestureRecognizer(doubleTap)
    }
    
    func setupTapGesturizer(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(ViewPhoto.handleTap(_:)))
        tap.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tap)
    }
    
    func handleDoubleTap(recognizer: UITapGestureRecognizer) {
        
        if (scrollView.zoomScale > scrollView.minimumZoomScale) {
            scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
        } else {
            scrollView.setZoomScale(scrollView.maximumZoomScale, animated: true)
        }
    }
    
    func loadFullSizeImage(){
        let option = PHImageRequestOptions()
        option.networkAccessAllowed = true
        option.deliveryMode = .HighQualityFormat
        
        option.progressHandler = {(progress: Double,
            err: NSError?,
            stop: UnsafeMutablePointer<ObjCBool>,
            info: [NSObject : AnyObject]?) in
            
            if let error = err{
                print("Error accured: \(error)", terminator: "")
            }
            else{
                dispatch_async(dispatch_get_main_queue(), {
                    self.progressBar.progress = Float(progress)
                    
                    if progress >= 1 {
                        let delayInSeconds = 0.5
                        let popTime = dispatch_time(DISPATCH_TIME_NOW, Int64(delayInSeconds * Double(NSEC_PER_SEC)))
                        
                        dispatch_after(popTime, dispatch_get_main_queue()) {
                            self.progressBar.hidden = true
                        }
                    }
                    else{
                        self.progressBar.hidden = false
                    }
                })
            }
            
        }
        
        manager.requestImageForAsset(self.asset, targetSize: PHImageManagerMaximumSize, contentMode: PHImageContentMode.AspectFit, options: option, resultHandler: { (image, info) in
            if image == nil{
                self.PresentErrorMessage(info!)
            }
            else{
                self.imageView = UIImageView(image: image)
                
                self.scrollView = UIScrollView(frame: self.view.bounds)
                self.scrollView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
                
                self.scrollView.addSubview(self.imageView)
                self.view.addSubview(self.scrollView)
                
                self.scrollView.delegate = self
                
                self.setZoomScale()
                self.setupGestureRecognizer()
            }
        })
    }
    
    func PresentErrorMessage(info:NSDictionary){
        let errormessage = info.objectForKey(PHImageErrorKey) as? NSError
        let alertController = UIAlertController(title: "Error", message: "Error while downloading photo: \(errormessage?.localizedDescription)", preferredStyle: .Alert)
        
        let okAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            print(action)
        }
        alertController.addAction(okAction)
        
        self.presentViewController(alertController, animated: true) {
            // ...
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
    @IBOutlet weak var progressBar: UIProgressView!
    var hidden = false
    var asset: PHAsset!
    var photoCollection = [Int: [Int]]()
    var year:Int = -1
    var row:Int = -1
    let manager = PHImageManager.defaultManager()
    var fetchResult: PHFetchResult!
}
