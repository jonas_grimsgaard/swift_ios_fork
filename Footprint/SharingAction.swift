//
//  SharingAction.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 29.08.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import UIKit
import Photos

class SharingAction {
    
    func shareAction(asset: PHAsset, viewController:UIViewController ){
        let requestOptions = PHImageRequestOptions()
        requestOptions.networkAccessAllowed = true
        requestOptions.synchronous = true
        
        PHImageManager.defaultManager().requestImageForAsset(asset, targetSize: PHImageManagerMaximumSize, contentMode: PHImageContentMode.AspectFill, options: requestOptions, resultHandler: { (image, _) in
            
            if let photo = image{
                
                var message = ""
                
                if let date = asset.creationDate{
                    
                    let formatter = NSDateFormatter()
                    formatter.dateFormat = "Y"
                    if let year = Int(formatter.stringFromDate(date)){
                        if let today = Int(formatter.stringFromDate(NSDate())){
                            let yearTitle = (year > 1) ? "years" : "year"
                            let years = today - year
                            message = "This memory happened \(years) \(yearTitle) ago"
                        }
                    }
                }
                let shareItems:Array = [photo, message]
                let activityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
                activityViewController.excludedActivityTypes = [UIActivityTypePrint, UIActivityTypePostToWeibo, UIActivityTypeAddToReadingList, UIActivityTypePostToVimeo]
                viewController.presentViewController(activityViewController, animated: true, completion: nil)
            }
        })
    }
}