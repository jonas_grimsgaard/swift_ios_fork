//
//  PhotosController.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 01.08.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import UIKit
import Photos

let reuseIdentifier = "photoCell"

class PhotosController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var fetchResult: PHFetchResult!
    let imgManager = PHCachingImageManager()
    let dailyPhotosLogic = DailyPhotoLogic()
    var photoSearched = 0
    var photoCollection = [Int: [Int]]()
    
    @IBOutlet weak var disableDailyPhotoButton: UIButton!
    @IBAction func disableDailyPhotos(sender: AnyObject) {
        
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return photoCollection.count
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        let keys = getKeys()
        return photoCollection[keys[section]]!.count
    }
    
    let requestOptions = PHImageRequestOptions()
    let cachingImageManager = PHCachingImageManager()
    
    // The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! PhotoCell
        
        imgManager.requestImageForAsset(assetFromIndexPath(indexPath), targetSize: CGSize(width: 250, height: 250), contentMode: PHImageContentMode.AspectFit, options: requestOptions, resultHandler: { (image, _) in
            if let photo = image{
                cell.setThumbnailImage(photo)
            }
            else
            {
                print("Something is wrong with this image: \(self.assetFromIndexPath(indexPath))")
            }
        })
        
        return cell
    }
    
    func assetFromIndexPath(indexPath:NSIndexPath) -> PHAsset{
        let keys = getKeys()
        let photoElements = self.photoCollection[keys[indexPath.section]]
        
        return fetchResult[photoElements![indexPath.row]] as! PHAsset
    }
    
    func fetchPhotos () {
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            
            var assets = [PHAsset]()
            for index in 0...(self.fetchResult.count - 1){
                let asset = self.fetchResult[index] as! PHAsset
                if self.dailyPhotosLogic.shouldImageBeIncluded(asset){
                    assets.append(asset)
                    let formatter = NSDateFormatter()
                    formatter.dateFormat = "Y"
                    let year = Int(formatter.stringFromDate(asset.creationDate!))
                    
                    if let found = self.photoCollection[year!] {
                        var value = found
                        value.append(index)
                        self.photoCollection.updateValue(value, forKey: year!)
                    }
                    else{
                        self.photoCollection[year!] = [index]
                    }
                    dispatch_async(dispatch_get_main_queue()) {
                        self.collectionView.reloadData()
                        self.disableDailyPhotoButton.hidden = true
                    }
                }
            }
            
            self.cachingImageManager.startCachingImagesForAssets(assets, targetSize: PHImageManagerMaximumSize, contentMode: .AspectFit, options: nil)
            
            self.refreshControl.endRefreshing()
        }
    }
    
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: false)]
        fetchResult = PHAsset.fetchAssetsWithOptions(fetchOptions)
        
        requestOptions.synchronous = true
        requestOptions.resizeMode = .Fast
        requestOptions.networkAccessAllowed = true
        requestOptions.deliveryMode = .HighQualityFormat
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(PhotosController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.collectionView.addSubview(refreshControl)
        
        fetchPhotos()
        
        if let state = plistManager.getValueFromInfoList(dailyPhotosKey) as? Bool{
            reloadState = state
        }
    }
    
    func refresh(sender:AnyObject){
        self.photoCollection.removeAll()
        collectionView.reloadData()
        fetchPhotos()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        if let newState = plistManager.getValueFromInfoList(dailyPhotosKey) as? Bool{
            if newState != reloadState {
                reloadState = newState
                photoCollection.removeAll()
                collectionView.reloadData()
                fetchPhotos()
            }
        }
    }
    
    var reloadState = false
    let plistManager = PListManager()
    let dailyPhotosKey = "ShouldShowDailyPhotos"
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "viewLargePhoto"{
            if let navController = segue.destinationViewController as? UINavigationController{
                if let controller:ViewPhoto = navController.topViewController as? ViewPhoto{
                    let indexPath:NSIndexPath = self.collectionView.indexPathForCell(sender as! UICollectionViewCell)!
                    let keys = getKeys()
                    controller.asset = assetFromIndexPath(indexPath)
                    controller.photoCollection = photoCollection
                    controller.year = keys[indexPath.section]
                    controller.row = indexPath.row
                    controller.fetchResult = fetchResult
                }
            }
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        switch kind {
            //2
        case UICollectionElementKindSectionHeader:
            //3
            if let headerView =
                collectionView.dequeueReusableSupplementaryViewOfKind(kind,
                    withReuseIdentifier: "ImageCollectionHeader",
                    forIndexPath: indexPath)
                    as? ImageCollectionHeader{
                        let keys = getKeys()
                        
                        headerView.headerLabel.text = "\(getHeaderText(keys[indexPath.section]))"
                        return headerView
            }
        default:
            //4
            assert(false, "Unexpected element kind")
        }
        return ImageCollectionHeader()
    }
    
    func getKeys() -> [Int]{
        return ([Int](self.photoCollection.keys)).sort(>)
    }
    
    func getHeaderText(inputYear: Int) -> String{
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "Y"
        let year = Int(formatter.stringFromDate(NSDate()))
        
        if inputYear == year{
            return "\(year!)"
        }
        if (year! - inputYear) == 1 {
            return "1 year ago"
        }
        return "\(year! - inputYear) years ago"
    }
}
