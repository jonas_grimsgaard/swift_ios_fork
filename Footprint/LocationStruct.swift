//
//  LocationStruct.swift
//  Footprint
//
//  Created by Jonas Bo Grimsgaard on 27.10.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import Foundation
import MapKit

struct LocationStructed {
    var Name:NSString?
    var Thoroughfare:NSString?
    var City:NSString?
    var Zip:NSString?
    var Country:NSString?
    
    init(location:CLLocation){
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        geoCoder.reverseGeocodeLocation(location)
            {
                (placemarks, error) -> Void in
                
                if let placeArray = placemarks as [CLPlacemark]?{
                    // Place details
                    var placeMark: CLPlacemark!
                    placeMark = placeArray[0]
                    
                    self.Name = placeMark.addressDictionary!["Name"] as? NSString
                    self.Thoroughfare = placeMark.addressDictionary!["Thoroughfare"] as? NSString
                    self.City = placeMark.addressDictionary!["City"] as? NSString
                    self.Zip = placeMark.addressDictionary!["Zip"] as? NSString
                    self.Country = placeMark.addressDictionary!["Country"] as? NSString
                }
        }
    }
    
    func equal(loc:LocationStructed) -> Bool{
        return loc.Thoroughfare == self.Thoroughfare &&
            loc.City == self.City &&
            loc.Zip == self.Zip &&
            loc.Country == self.Country
    }
    /*
    mutating func setLocationNameFromLocation(_point1: CLLocation)
    {
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: _point1.coordinate.latitude, longitude: _point1.coordinate.longitude)
        geoCoder.reverseGeocodeLocation(location)
            {
                (placemarks, error) -> Void in
                
                if let placeArray = placemarks as [CLPlacemark]?{
                    
                    // Place details
                    var placeMark: CLPlacemark!
                    placeMark = placeArray[0]
                    
                    // Address dictionary
                    //print(placeMark.addressDictionary)
                    
                    // Location name
                    if let locationName = placeMark.addressDictionary?["Name"] as? NSString
                    {
                        self.Name = locationName as String
                        print(locationName)
                    }
                    
                    // Street address
                    if let street = placeMark.addressDictionary?["Thoroughfare"] as? NSString
                    {
                        self.Thoroughfare = street as String
                        print(street)
                    }
                    
                    // City
                    if let city = placeMark.addressDictionary?["City"] as? NSString
                    {
                        self.City = city as String
                        print(city)
                    }
                    
                    // Zip code
                    if let zip = placeMark.addressDictionary?["ZIP"] as? NSString
                    {
                        self.Zip = zip as String
                        print(zip)
                    }
                    
                    // Country
                    if let country = placeMark.addressDictionary?["Country"] as? NSString
                    {
                        self.Country = country as String
                        print(country)
                    }
                }
        }
    }*/
}