//
//  Node.swift
//  Footprint
//
//  Created by Jonas Bo Grimsgaard on 06.11.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import Foundation

class Node {
    var data: AnyObject?
    var children: [Node] = [Node]()
    
    func isLeaf() -> Bool{
        if children.count == 0
        { return true }
        return false
    }
    
    func leafNodesCount() -> Int{
        var count = 0
        
        if isLeaf(){
            count += 1
        }
        else{
            for child in children{
                count += child.leafNodesCount()
            }
        }
        
        return count
    }
    
    func leafNodes() -> [Node]{
        var nodes = [Node]()
        
        if isLeaf(){
            nodes.append(self)
        }
        else{
            for child in children{
                let leaves = child.leafNodes()
                for leaf in leaves{
                    nodes.append(leaf)
                }
            }
        }
        
        return nodes
    }
    
    init(object: AnyObject){
        data = object
    }
    
    func addChild(child:Node){
        self.children.append(child)
    }
    
    init(){}
}