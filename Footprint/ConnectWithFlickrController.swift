//
//  ConnectWithFlickrController.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 01.08.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import UIKit

class ConnectWithFlickrController: UIViewController {

    @IBAction func closeWindow(sender: UIButton) {
        //navigationController?.popViewControllerAnimated(true)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let request = NSURLRequest(URL: NSURL(fileURLWithPath: "http://flickr.com"))
        
        webView.loadRequest(request)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
