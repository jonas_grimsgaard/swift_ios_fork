//
//  DailyPhotoLogic.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 23.08.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import UIKit
import Photos

class DailyPhotoLogic {
    
    let plistManager = PListManager()
    let dailyPhotosKey = "ShouldShowDailyPhotos"
    
    func shouldImageBeIncluded(asset: PHAsset) -> Bool{
        if let dailyPhoto = plistManager.getValueFromInfoList(dailyPhotosKey) as? Bool{
            
            if !dailyPhoto{
                return true
            }
            if let inputDate = asset.creationDate{
                if dailyPhoto{
                    let formatter = NSDateFormatter()
                    formatter.dateFormat = "MM-dd"
                    if formatter.stringFromDate(NSDate()) == formatter.stringFromDate(inputDate){
                        return true
                    }
                    return false
                }
            }
        }
        return true
    }
}