//
//  SettingsViewController.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 23.08.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    let plistManager = PListManager()
    let dailyPhotosKey = "ShouldShowDailyPhotos"
    
    override func viewDidAppear(animated: Bool) {
        setDailySwitchValueFromPlistManager()
    }
    
    func setDailySwitchValueFromPlistManager(){
        if let dailySwitch = plistManager.getValueFromInfoList(dailyPhotosKey) as? Bool{
            viewDailyPhotosSwitch.setOn(dailySwitch, animated: false)
        }
    }
    
    @IBAction func changeDailyPhotoSettings(sender: UISwitch) {
        plistManager.saveData(dailyPhotosKey, value: sender.on as Bool)
    }
    
    @IBOutlet weak var viewDailyPhotosSwitch: UISwitch!
    
    override func viewDidLoad() {
        setDailySwitchValueFromPlistManager()
    }
}
