//
//  FetchVideos.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 06.09.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import UIKit
import Photos

class FetchVideosManager
{
    func getVideo(){
        let options = PHVideoRequestOptions()
        options.networkAccessAllowed = true
        options.progressHandler = {(progress: Double,
            err: NSError?,
            stop: UnsafeMutablePointer<ObjCBool>,
            info: [NSObject : AnyObject]?) in
            
            if let error = err{
                print("Error accured: \(error)", terminator: "")
            }
            
        }
    }
    
    func startCachingVideos(){
       /* let cachingImageManager = PHCachingImageManager()
        
        let options = PHFetchOptions()
        options.sortDescriptors = [
            NSSortDescriptor(key: "creationDate", ascending: true)
        ]
        
        if let results = PHAsset.fetchAssetsWithMediaType(.Image, options: options) {
        var assets: [PHAsset] = [] {
            willSet {
                cachingImageManager.stopCachingImagesForAllAssets()
            }
            
            didSet {
                cachingImageManager.startCachingImagesForAssets(assets, targetSize: PHImageManagerMaximumSize, contentMode: .AspectFit, options: nil)
            }
        }
        }
        */
    }
}
