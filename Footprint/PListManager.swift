//
//  LoadFromInfoList.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 23.08.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import Foundation

class PListManager{
    
    func getValueFromInfoList(key: String) -> AnyObject? {
        /*if let path = NSBundle.mainBundle().pathForResource("Info", ofType: "plist") {
            let dict = NSDictionary(contentsOfFile: path)
            if let token = dict?.objectForKey(key){
                return token
            }
        }
        return nil*/
        
        let defaults = NSUserDefaults.standardUserDefaults()
        return defaults.objectForKey(key)
    }
    
    func saveData(key: String, value: AnyObject){
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(value, forKey: key)
    }
}