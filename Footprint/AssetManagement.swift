//
//  AssetManagement.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 07.08.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//
import Foundation
import Photos

class AssetManagement {
    
    let fetchResult: PHFetchResult!
    let imgManager = PHImageManager.defaultManager()
    
    init(){
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: true)]
        
        fetchResult = PHAsset.fetchAssetsWithMediaType(PHAssetMediaType.Image, options: fetchOptions)
    }
    
    func getAssetsWithLocationData(completion: ([PHAsset]) -> Void){
        let requestOptions = PHImageRequestOptions()
        requestOptions.synchronous = true
        requestOptions.resizeMode = .Exact
        requestOptions.networkAccessAllowed = true
        requestOptions.deliveryMode = .HighQualityFormat
        
        var images = [PHAsset]()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
            for index in 0..<self.fetchResult.count {
                // Perform the image request
                let asset = self.fetchResult[index] as! PHAsset
                
                if asset.location != nil {
                    images.append(asset)
                }
            }
            completion(images)
        })
    }
    
    func indexOfObjectInAsset(asset:PHAsset) -> Int{
        return fetchResult.indexOfObject(asset)
    }
}