//
//  TabBarController.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 14.08.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        self.tabBar.items?[0].title = "Photos"
        self.tabBar.items?[1].title = "Map"
        self.tabBar.items?[2].title = "Settings"
    }
}
