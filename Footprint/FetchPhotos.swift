//
//  FetchPhotos.swift
//  Footprint
//
//  Created by Jonas Grimsgaard on 04.08.2015.
//  Copyright © 2015 Jonas Grimsgaard. All rights reserved.
//

import Foundation
import Photos

class FetchPhotos {
    
    
    
    func getFetchResults() -> PHFetchResult{
        return PHAsset.fetchAssetsWithMediaType(PHAssetMediaType.Image, options: getFetchOptions())
    }
    
    func getFetchOptions() -> PHFetchOptions{
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: true)]
        //fetchOptions.predicate = NSPredicate(format: <#T##String#>, <#T##args: CVarArgType...##CVarArgType#>)
        return fetchOptions
    }
    
    func getFetchRequest() -> PHImageRequestOptions{
        let requestOptions = PHImageRequestOptions()
        requestOptions.synchronous = true
        return requestOptions
    }
}